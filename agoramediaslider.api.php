<?php

/**
 * @file
 * Hooks provided by the agoramediaslider module.
 */

/**
 * Alter the result of agoramediaslider_allowed_slide_template_values().
 *
 * @param string[] &$templates
 *   An array containing the template IDs as keys and the labels as values.
 */
function hook_agoramediaslider_allowed_slide_template_values_alter(array &$templates) {
  $templates['custom'] = t('Custom');
}
