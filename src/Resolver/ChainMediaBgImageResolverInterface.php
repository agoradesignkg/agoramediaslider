<?php

namespace Drupal\agoramediaslider\Resolver;

/**
 * Runs the added resolvers one by one until one of them returns the bg image.
 *
 * Each resolver in the chain can be another chain, which is why this interface
 * extends the base media background image resolver one.
 */
interface ChainMediaBgImageResolverInterface extends MediaBgImageResolverInterface {

  /**
   * Adds a resolver.
   *
   * @param \Drupal\agoramediaslider\Resolver\MediaBgImageResolverInterface $resolver
   *   The resolver.
   */
  public function addResolver(MediaBgImageResolverInterface $resolver);

  /**
   * Gets all added resolvers.
   *
   * @return \Drupal\agoramediaslider\Resolver\MediaBgImageResolverInterface[]
   *   The resolvers.
   */
  public function getResolvers();

}
