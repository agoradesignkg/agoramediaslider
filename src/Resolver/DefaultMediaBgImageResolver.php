<?php

namespace Drupal\agoramediaslider\Resolver;

use Drupal\media\MediaInterface;

/**
 * Returns the background image based on the media bundle and a specific field.
 */
class DefaultMediaBgImageResolver implements MediaBgImageResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(MediaInterface $entity) {
    $url = NULL;
    $bg_image_bundles = ['slide_default'];
    if (!empty($entity) && $entity->hasField('field_media_image') && !$entity->get('field_media_image')->isEmpty() && in_array($entity->bundle(), $bg_image_bundles)) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $entity->field_media_image->entity;
      $url = $file->createFileUrl(FALSE);
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(MediaInterface $entity) {
    return TRUE;
  }

}
