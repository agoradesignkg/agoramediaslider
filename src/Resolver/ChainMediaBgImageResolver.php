<?php

namespace Drupal\agoramediaslider\Resolver;

use Drupal\media\MediaInterface;

/**
 * Default implementation of the chain media bg image resolver.
 */
class ChainMediaBgImageResolver implements ChainMediaBgImageResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Drupal\agoramediaslider\Resolver\MediaBgImageResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Constructs a new ChainBasePriceResolver object.
   *
   * @param \Drupal\agoramediaslider\Resolver\MediaBgImageResolverInterface[] $resolvers
   *   The resolvers.
   */
  public function __construct(array $resolvers = []) {
    $this->resolvers = $resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function addResolver(MediaBgImageResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolvers() {
    return $this->resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(MediaInterface $entity) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($entity)) {
        return $resolver->resolve($entity);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies(MediaInterface $entity) {
    return TRUE;
  }

}
