<?php

namespace Drupal\agoramediaslider\Resolver;

use Drupal\media\MediaInterface;

/**
 * Defines the interface for media entity background image resolvers.
 *
 * The existence of this interface has a rather unfortunate reason. The problem
 * is, that we're "over-reusing" the field_media_image field. For slides with an
 * image we automatically assume that the image field should be rendered as
 * background image. But actually, when the field is used in normal image media
 * types, this is definitely unwanted behaviour.
 *
 * As in some customer projects we may want to override this default rule, we
 * have built the resolver chain, to be flexible enough for any requirements.
 */
interface MediaBgImageResolverInterface {

  /**
   * Resolves the background image url of a given media entity.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The media entity.
   *
   * @return string
   *   A string containing the background image URL, if resolved. An empty
   *   string can be returned in case the resolver is responsible for
   *   determining the background image, but couldn't find one, e.g. empty image
   *   field, etc.
   */
  public function resolve(MediaInterface $entity);

  /**
   * Whether this resolver is responsible to decide on resolving a bg image.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The media entity.
   *
   * @return bool
   *   TRUE, if the resolver takes over responsibility to decide on resolving
   *   the background image url, even if finally none could be provided.
   *   Otherwise FALSE, indicating that the next resolver in the chain should be
   *   called.
   */
  public function applies(MediaInterface $entity);

}
